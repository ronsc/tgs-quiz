import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div style="padding: 0 24px;">
      <router-outlet></router-outlet>
    </div>
  `
})
export class AppComponent {}
