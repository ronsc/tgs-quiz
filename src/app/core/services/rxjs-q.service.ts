import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { RxjsQ2Data, RxjsQ3Data } from '../models/rxjs-q.model';

export const baseUrl = 'https://jsonplaceholder.typicode.com';

@Injectable({
  providedIn: 'root'
})
export class RxjsQService {
  private readonly todoEndpoint = `${baseUrl}/todos`;
  private readonly userEndpoint = `${baseUrl}/users`;
  private readonly postEndpoint = `${baseUrl}/posts`;

  constructor(private http: HttpClient) {}

  // TODO: Quiz 2 replace this mock.
  fetchInitData(): Observable<RxjsQ2Data> {
    return this.getMockRxjsQ2();
  }

  // TODO: Quiz 3 replace this mock.
  queryTodoByUser(): Observable<RxjsQ3Data> {
    return this.getMockRxjsQ3();
  }

  getMockRxjsQ2(): Observable<RxjsQ2Data> {
    return this.http.get<RxjsQ2Data>('assets/mocks/rxjs-q2.output.json');
  }

  getMockRxjsQ3(): Observable<RxjsQ3Data> {
    return this.http.get<RxjsQ3Data>('assets/mocks/rxjs-q3.output.json');
  }
}
