import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { RxjsQ3Data } from 'src/app/core/models/rxjs-q.model';
import { RxjsQService } from 'src/app/core/services/rxjs-q.service';

@Component({
  selector: 'app-rxjs-q3',
  templateUrl: './rxjs-q3.component.html',
  styles: []
})
export class RxjsQ3Component implements OnInit {
  rxjsQ3$: Observable<RxjsQ3Data>;
  rxjsQ3Output$ = this.rxjsQ2Service.getMockRxjsQ3();

  constructor(private rxjsQ2Service: RxjsQService) {}

  ngOnInit() {}

  queryTodoByUser(): void {
    this.rxjsQ3$ = this.rxjsQ2Service.queryTodoByUser();
  }
}
