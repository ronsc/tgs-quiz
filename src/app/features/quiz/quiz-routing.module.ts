import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { QuizComponent } from './quiz/quiz.component';
import { RxjsQ1Component } from './rxjs-q1/rxjs-q1.component';
import { RxjsQ2Component } from './rxjs-q2/rxjs-q2.component';
import { RxjsQ3Component } from './rxjs-q3/rxjs-q3.component';
import { RxjsComponent } from './rxjs/rxjs.component';

const routes: Routes = [
  {
    path: '',
    component: QuizComponent,
    children: [
      { path: '', pathMatch: 'full', component: RxjsComponent },
      { path: 'rxjs-q1', component: RxjsQ1Component },
      { path: 'rxjs-q2', component: RxjsQ2Component },
      { path: 'rxjs-q3', component: RxjsQ3Component }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuizRoutingModule {}
