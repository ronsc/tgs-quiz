import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';

import { QuizRoutingModule } from './quiz-routing.module';
import { QuizComponent } from './quiz/quiz.component';
import { RxjsQ1Component } from './rxjs-q1/rxjs-q1.component';
import { RxjsQ2Component } from './rxjs-q2/rxjs-q2.component';
import { RxjsQ3Component } from './rxjs-q3/rxjs-q3.component';
import { RxjsComponent } from './rxjs/rxjs.component';

@NgModule({
  declarations: [
    RxjsComponent,
    RxjsQ2Component,
    RxjsQ3Component,
    RxjsQ1Component,
    QuizComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,

    FlexLayoutModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,

    QuizRoutingModule
  ]
})
export class QuizModule {}
