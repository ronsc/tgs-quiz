import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

@Component({
  selector: 'app-rxjs-q1',
  templateUrl: './rxjs-q1.component.html',
  styles: []
})
export class RxjsQ1Component implements OnInit {
  formGroup = this.fb.group({
    salePrice: [],
    downPrice: [],
    remainPrice: [{ value: 0, disabled: true }],
    noInstallments: [],
    installmentPrice: [{ value: 0, disabled: true }]
  });

  constructor(private fb: FormBuilder) {}

  ngOnInit() {}
}
