import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { RxjsQ2Data } from 'src/app/core/models/rxjs-q.model';
import { RxjsQService } from 'src/app/core/services/rxjs-q.service';

@Component({
  selector: 'app-rxjs-q2',
  templateUrl: './rxjs-q2.component.html',
  styles: []
})
export class RxjsQ2Component implements OnInit {
  rxjsQ21$: Observable<RxjsQ2Data>;
  rxjsQ21Output$ = this.rxjsQ2Service.getMockRxjsQ2();

  constructor(private rxjsQ2Service: RxjsQService) {}

  ngOnInit() {}

  fetchInitData(): void {
    this.rxjsQ21$ = this.rxjsQ2Service.fetchInitData();
  }
}
