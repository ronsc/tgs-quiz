import { AfterViewInit, Component } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { bufferWhen, filter, map, take, tap } from 'rxjs/operators';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styleUrls: ['./rxjs.component.scss']
})
export class RxjsComponent implements AfterViewInit {
  form: FormGroup = this.fb.group({
    checkboxs: this.fb.array([
      this.fb.control(false),
      this.fb.control(false),
      this.fb.control(false),
      this.fb.control(false)
    ]),
    result: [[]]
  });

  constructor(private fb: FormBuilder) {}

  ngAfterViewInit(): void {
    this.groupByBufferWhen();
  }

  get checkboxs(): FormArray {
    return this.form.get('checkboxs') as FormArray;
  }

  get result(): FormControl {
    return this.form.get('result') as FormControl;
  }

  groupByBufferWhen(): void {
    const groupBtnClick = () => {
      return fromEvent(document.getElementById('groupBtn'), 'click');
    };
    const isValid = (cbValue: boolean[]) => {
      return !!cbValue && cbValue.includes(true);
    };
    const validate = (cbValue: boolean[]) => {
      if (!isValid(cbValue)) {
        alert('Please checked at lease 1.');
      }
    };

    this.checkboxs.valueChanges
      .pipe(
        bufferWhen(groupBtnClick),
        map(bufferValues => bufferValues.pop()),
        tap(validate),
        filter(isValid),
        take(3)
      )
      .subscribe({
        next: (cbValue: boolean[]) => {
          const result = cbValue
            .map((checked, i) => ({ name: `A${i + 1}`, checked }))
            .filter(cb => cb.checked)
            .map(cb => cb.name)
            .join(', ');

          this.updateResult(result);
          this.clearCheckboxs();
        },
        complete: () => alert('Group Completed!')
      });
  }

  private updateResult(result: string): void {
    const { value = [] } = this.result;

    this.result.setValue([...value, result]);
  }

  private clearCheckboxs(): void {
    this.checkboxs.reset([false, false, false, false], {
      emitEvent: false
    });
  }
}
